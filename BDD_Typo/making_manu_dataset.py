# -*- coding: utf-8 -*-
"""
Created on Mon Oct 21 13:50:57 2019

COMPATIBILITE WINDOWS SEULEMENT !!

@author: piotm
"""
import subprocess
import os
import random
import datetime

def recupFontsListe():
    with open ('./MagickFontsListe.txt', 'w+') as f:
        subprocess.run('magick -list font | findstr Font:', stdout=f)
        f.seek(0)
        lines = []
        for l in f.readlines():
            if '  Font:' in l :
                l=l.replace('\n', '')
                l=l.replace('  Font: ', '')
                lines.append(l)
    f.close()
    with open('./TrashFonts.txt', 'r') as t :
        for i in t.readlines():
            i=i.split(' ')[0]
            del lines[lines.index(i)]
    t.close()
    return lines
           
def runCommand(name_image, font, digit, rot):
    with open('TmpErrors.txt', 'w+') as f :
        subprocess.run('magick convert output.jpg -size 28x28 -gravity center -fill black \
                                   -font %s -pointsize 22 \
                                   -annotate 1x%d %d  \
                                   -blur 0x0.2 \
                                   -colorspace gray \
                                   -channel gray ./%s/%s.jpeg'%(font, rot,
                                   digit, str(digit), str(name_image) + '_' + font), stdout=f)        
        f.seek(0)
        l = f.readline()
        if l != '':
            print('ERROR : %s'%l)
            print('Image concernee : %d'%name_image)
            with open('Errors.txt', 'a') as fe :
                fe.write(l)
                fe.write('\n')
                fe.write(name_image)
                fe.write('\n')
            fe.close()
    f.close()
    

def creationDataset():
    # Initialisation
    fonts = recupFontsListe()
    digits = range(0,10)
    positive_rotation = range(2, 13, 2)
    negative_rotation = range(348, 359, 2) #-2 à -12 degres
    background = './input.jpg'
    
    # Construction du fond de taille 28x28
    subprocess.run('magick convert %s -crop 28x28+0+0 output.jpg'%background)
    print("Background : OK")
    
    # Generation des dossiers
    for digit in digits :
        try :
            os.mkdir('./%s'%str(digit))
        except(FileExistsError) :
            pass
    print("Dossiers : OK")
    
    # Generation de la base
    name_image = 0
    start=datetime.datetime.now()
    print('[%s] Creation du dataset...'%str(start))
    for font in fonts :
        for digit in digits :
            pr = random.choice(positive_rotation)
            nr = random.choice(negative_rotation)
            
            runCommand(name_image, font, digit, 0)
            name_image+=1
            
            runCommand(name_image, font, digit, pr)
            name_image+=1
            
            runCommand(name_image, font, digit, nr)
            name_image+=1
    
    stop=datetime.datetime.now()
    print('Dataset : OK')
    print('Duree : %s'%str(stop-start))
    
    for digit in digits :
        nb_elements = os.listdir('./%d'%digit)
        print("Taille classe %d : %d elements"%(digit,len(nb_elements)))
        
creationDataset()
