# -*- coding: utf-8 -*-
"""
Created on Wed Oct 23 15:43:37 2019

@author: Z240
"""

import numpy as np
from matplotlib.image import imread
from matplotlib import pyplot as plt
from sklearn.utils import shuffle
import os
import random
from sklearn.model_selection import train_test_split as split

DATA_TRAIN = 'data_train.bin'
DATA_TEST = 'data_test.bin'
LABEL_TRAIN = 'label_train.bin'
LABEL_TEST = 'label_test.bin'


classes = range(0,10)

def shuffleDatasets():
    def shuffleDataset(data, label):
        global arr_data, arr_label
        with open(data, "r") as bin_f_data:
            with open(label, "r") as bin_f_label:
                arr_data = np.fromfile(bin_f_data, dtype='float64')
                size = len(arr_data)//(28*28)
                arr_data = arr_data.reshape(size,28*28)
                
                arr_label = np.fromfile(bin_f_label, dtype='uint8')#.reshape(size,10)
                
                arr_data, arr_label = shuffle(arr_data, arr_label, random_state = random.randint(1,50))
                
                with open(data, "w") as bin_f_data_shuff:
                    arr_data.tofile(bin_f_data_shuff)
                with open(label, "w") as bin_f_label_shuff:
                    arr_label.tofile(bin_f_label_shuff)


    shuffleDataset(DATA_TRAIN, LABEL_TRAIN)
    shuffleDataset(DATA_TEST, LABEL_TEST)

def affichage(c):
    images = os.listdir('./%d'%c)
    images = shuffle(images, random_state=random.randint(1,50))
    images = images[0:10]
    fig, axs = plt.subplots(2,5)
    fig.set_size_inches(5,2, forward=True)
    for i in range(0,10) :
        axs[i//5,i%5].imshow(imread('./%d/%s'%(c,images[i])), cmap = 'gray')
        axs[i//5,i%5].axis('off')
    
    plt.show()

def removeFile(f):
    try:
        os.remove(f)
    except(FileNotFoundError):
        pass
    
def writeBin():
    removeFile(DATA_TRAIN)
    removeFile(DATA_TEST)
    removeFile(LABEL_TRAIN)
    removeFile(LABEL_TEST)
    
    for c in classes :
        samples = os.listdir('./%d'%c)
        train_samples, test_samples = split(samples, 
                                            train_size=0.8, 
                                            test_size=0.2, 
                                            shuffle=True) 

        
        #affichage(train_samples, c)
        
        with open(DATA_TRAIN, "ab") as bin_f:
            labels_train=[]
            for s in train_samples : 
                #img = PreprocessImage(imread('./%d/%s'%(c,s)))
                bin_f.write(imread('./%d/%s'%(c,s))/255)
                labels_train.append(c)

        #labels_train = one_hot(labels_train, num_labels=len(classes))           
        with open(LABEL_TRAIN, "ab") as bin_l:
            bin_l.write(np.array(labels_train, dtype='uint8'))
            
            
        with open(DATA_TEST, "ab") as bin_f:
            labels_test=[]
            for s in test_samples : 
                #img = PreprocessImage(imread('./%d/%s'%(c,s)))
                bin_f.write(imread('./%d/%s'%(c,s))/255)
                labels_test.append(c)
        
        #labels_test = one_hot(labels_test, num_labels=len(classes))           
        with open(LABEL_TEST, "ab") as bin_l:
            bin_l.write(np.array(labels_test, dtype='uint8'))
            
    
    shuffleDatasets()
    
    

def readBin():
    
    def showDigits(file):
        with open(file, "r") as bin_f:
        
            arr = np.fromfile(bin_f, dtype='float64')
            size = len(arr)//(28*28)
            class_size = size // 10
            arr = arr.reshape(size,28*28)
            
            print('Total data in %s : %d'%(file, size))
            print('Total data per class : %d'%(class_size))
            fig, axs = plt.subplots(2,5)
            fig.set_size_inches(5,2, forward=True)
            for i in range(0,10) :
                axs[i//5,i%5].imshow(arr[i*class_size].reshape(28,28), cmap = 'gray')
                axs[i//5,i%5].axis('off')
            
            plt.show()
    
    showDigits(DATA_TEST)
    showDigits(DATA_TRAIN)
    
    





        
        




