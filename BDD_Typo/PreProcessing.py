# -*- coding: utf-8 -*-
"""
Created on Fri Oct 25 12:32:24 2019

@author: bourd
"""
import numpy as np
from matplotlib import pyplot as plt
import cv2 as cv


def PreprocessImage(img): 
    edges = cv.Canny(img,100,200)
    
    
    kernel = np.ones((5,5), np.uint8) 
    img_dilation = cv.dilate(edges, kernel, iterations=1)
    
    
    ctrs, hier = cv.findContours(img_dilation.copy(), cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE) 
    sorted_ctrs = sorted(ctrs, key=lambda ctr: cv.boundingRect(ctr)[0])
    
    x, y, w, h = cv.boundingRect(sorted_ctrs[0])
    
    # Getting ROI
    roi = img[y:y + h, x:x + w]
    sh = roi.shape
    roi_corr = np.ones(( sh[0] + 2, sh[1] + 2), dtype=np.uint8)
    roi_corr[1 : sh[0] + 1, 1 : sh[1] + 1] = roi[:][:]
    roi_corr[0, :] = roi[0, 0]
    roi_corr[-1, :] = roi[-1, -1]
    roi_corr[:, 0] = roi[-1, -1]
    roi_corr[:, -1] = roi[0, 0]
    
                
    crop_img = cv.resize(roi_corr, (28,28),interpolation = cv.INTER_AREA)
#    plt.imshow(crop_img, cmap = 'bone')
#    plt.show()
    
    return crop_img



    